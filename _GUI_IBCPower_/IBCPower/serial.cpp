#include "serial.h"

namespace serial {

Serial::Serial(QObject *parent) : QObject(parent) {
    serialDevice = new QSerialPort(this);
}

Serial::~Serial() {
    end(nullptr);
}

void Serial::run() { //from QRunnable
    QScopedPointer<QEventLoop> serloop(new QEventLoop);
    connect(this, &serial::Serial::disconnected, serloop.data(), &QEventLoop::quit);
    serloop->exec();
}

void Serial::addTstampString(QString &targetString) {
    switch (timestampMode) {
    case SysTimeStamp:
        targetString.insert(targetString.lastIndexOf("\r"),
                            "RDTSTAMP" + QString::number(tstClock.currentTime().msecsSinceStartOfDay()));
    }
}

void Serial::readString() {
    switch (stringReadMode) {
    case canReadLine_ReadLine:
        while (this->serialDevice->canReadLine()) {
            serialInputString.append(serialDevice->readLine());
            addTstampString(serialInputString);
        }
        break;
    case canReadLine_ReadAll: {
        QString tmp_read;
        while (this->serialDevice->canReadLine()) {
            tmp_read = serialDevice->readAll();
            addTstampString(tmp_read);
            serialInputString.append(tmp_read);
        }
        break;
    }
    case bytesAvailable_ReadLine: {
        QString tmp_read;
        while (this->serialDevice->bytesAvailable()) {
            tmp_read = serialDevice->readLine();
            if (tmp_read.startsWith("\n")) { tmp_read.remove(0, 1); }
            addTstampString(tmp_read);
            if (!tmp_read.isEmpty()) {
                serialInputString.append(tmp_read);
            }
        }
        break;
    }
    case bytesAvailable_ReadAll: {
        QString tmp_read;
        while (this->serialDevice->bytesAvailable()) {
            tmp_read = serialDevice->readAll();
            if (tmp_read.startsWith("\n")) { tmp_read.remove(0, 1); }
            addTstampString(tmp_read);
            if (!tmp_read.isEmpty()) {
                serialInputString.append(tmp_read);
            }
        }
        break;
    }
    }
    int lastNewLine = serialInputString.lastIndexOf("\r\n") + 2;
    if (lastNewLine > 2) {
        emit dataToParseReady(serialInputString.sliced(0, lastNewLine));
        serialInputString.remove(0, lastNewLine);
    }
}

bool Serial::setReadMode(int mode) {
    switch (mode) {
    case 0:
        stringReadMode = canReadLine_ReadLine;
        break;
    case 1:
        stringReadMode = canReadLine_ReadAll;
        break;
    case 2:
        stringReadMode = bytesAvailable_ReadLine;
        break;
    case 3:
        stringReadMode = bytesAvailable_ReadAll;
        break;
    default:
        return false;
    }
    return true;
}

QString Serial::getString(bool clearBuffer) {
    QString output = serialInputString;

    if (clearBuffer)
        clearString();

    return output;
}

void Serial::clearAll(bool clearHardwareBuffers) {
    serialInputString.clear();

    if (clearHardwareBuffers)
        serialDevice->clear();
}

void Serial::clearString() {
    serialInputString.clear();
}

QString Serial::getSerialInfo() {
    QString outputString;
    outputString.append(serialDevice->portName() + ", " + QString::number(serialDevice->baudRate()) + ", ");

    switch (serialDevice->dataBits()) {
    case QSerialPort::Data5:
        outputString.append("Data5, ");
        break;
    case QSerialPort::Data6:
        outputString.append("Data6, ");
        break;
    case QSerialPort::Data7:
        outputString.append("Data7, ");
        break;
    case QSerialPort::Data8:
        outputString.append("Data8, ");
        break;
    }

    switch (serialDevice->parity()) {
    case QSerialPort::NoParity:
        outputString.append("NoParity, ");
        break;
    case QSerialPort::OddParity:
        outputString.append("OddParity, ");
        break;
    case QSerialPort::EvenParity:
        outputString.append("EvenParity, ");
        break;
    case QSerialPort::MarkParity:
        outputString.append("MarkParity, ");
        break;
    case QSerialPort::SpaceParity:
        outputString.append("SpaceParity, ");
        break;
    }

    switch (serialDevice->stopBits()) {
    case QSerialPort::OneStop:
        outputString.append("OneStop, ");
        break;
    case QSerialPort::TwoStop:
        outputString.append("TwoStop, ");
        break;
    case QSerialPort::OneAndHalfStop:
        outputString.append("OneAndHalfStop, ");
        break;
    }

    switch (serialDevice->flowControl()) {
    case QSerialPort::NoFlowControl:
        outputString.append("NoFlowControl, ");
        break;
    case QSerialPort::HardwareControl:
        outputString.append("HardwareControl, ");
        break;
    case QSerialPort::SoftwareControl:
        outputString.append("SoftwareControl, ");
        break;
    }

    return outputString;
}

bool Serial::isOpen() {
    return serialDevice->isOpen();
}

QList<QSerialPortInfo> Serial::getAvailiblePorts() {
    return QSerialPortInfo::availablePorts();
}

QList<int> Serial::getAvailibleBaudRates() {
    return QSerialPortInfo::standardBaudRates();
}

bool Serial::begin(QString parsedPortName, qint32 parsedBaudRate, QString dataBits, QString parity, QString stopBits, QString flowControl, bool dtrOn) {
    if (QSerialPortInfo::availablePorts().count() < 1)
        return false;

    this->serialDevice->setPortName(parsedPortName);

    if (!serialDevice->isOpen()) {
        if (serialDevice->open(QSerialPort::ReadWrite)) {
            this->serialDevice->clear();
            this->serialDevice->setBaudRate(parsedBaudRate);

            if (dataBits.contains("Data5"))
                this->serialDevice->setDataBits(QSerialPort::Data5);
            else if (dataBits.contains("Data6"))
                this->serialDevice->setDataBits(QSerialPort::Data6);
            else if (dataBits.contains("Data7"))
                this->serialDevice->setDataBits(QSerialPort::Data7);
            else if (dataBits.contains("Data8"))
                this->serialDevice->setDataBits(QSerialPort::Data8);

            if (parity.contains("NoParity"))
                this->serialDevice->setParity(QSerialPort::NoParity);
            else if (parity.contains("EvenParity"))
                this->serialDevice->setParity(QSerialPort::EvenParity);
            else if (parity.contains("OddParity"))
                this->serialDevice->setParity(QSerialPort::OddParity);
            else if (parity.contains("SpaceParity"))
                this->serialDevice->setParity(QSerialPort::SpaceParity);
            else if (parity.contains("MarkParity"))
                this->serialDevice->setParity(QSerialPort::MarkParity);

            if (stopBits.contains("OneStop"))
                this->serialDevice->setStopBits(QSerialPort::OneStop);
            else if (stopBits.contains("OneAndHalfStop"))
                this->serialDevice->setStopBits(QSerialPort::OneAndHalfStop);
            else if (stopBits.contains("TwoStop"))
                this->serialDevice->setStopBits(QSerialPort::TwoStop);

            if (flowControl.contains("NoFlowControl"))
                this->serialDevice->setFlowControl(QSerialPort::NoFlowControl);
            else if (flowControl.contains("HardwareControl"))
                this->serialDevice->setFlowControl(QSerialPort::HardwareControl);
            else if (flowControl.contains("SoftwareControl"))
                this->serialDevice->setFlowControl(QSerialPort::SoftwareControl);

            this->serialDevice->setDataTerminalReady(dtrOn);

            connect(this->serialDevice, &QSerialPort::readyRead, this, &Serial::readString);

            return true;
        }
        return false;
    }
    else {
        return false;
    }
}

bool Serial::end(QObject *signalingObject, QThread *threadToMoveTo) {
    disconnect(serialDevice, &QSerialPort::readyRead, this, &Serial::readString);

    serialDevice->clear();
    serialDevice->close();

    if (threadToMoveTo != nullptr) {
        moveToThread(threadToMoveTo);
    }

    if (!this->serialDevice->isOpen()) {
        emit disconnected(true);
        return true;
    }
    else {
        emit disconnected(false);
        return false;
    }
}

bool Serial::send(QString message) {
    if (this->serialDevice->isOpen() && this->serialDevice->isWritable()) {
        this->serialDevice->write(message.toStdString().c_str());
        return true;
    }
    else {
        return false;
    }
}

// New function implementation
float Serial::convertStringToFloat(const QString &inputString) {
    bool ok;
    float value = inputString.toFloat(&ok);
    if (!ok) {
        qWarning() << "Conversion failed: Input string is not a valid float";
        // Handle the conversion error, for example, by returning a default value or throwing an exception
        return 0.0f;
    }
    return value;
}

void Serial::CanConvertFloatToBytes2(uint16_t *DestinationStartAddress, volatile float *inputData, float scaling, float offset) {
    volatile float tempVal, Gain;
    volatile uint16_t temp_uint;

#ifdef CCS
    DINT;  // Disable interrupts
#endif
    tempVal = *inputData; // Safe, atomic data acquisition
    Gain = scaling;
#ifdef CCS
    EINT;  // Enable interrupts
#endif

    // Replace _fastFloatDivide with direct division if _fastFloatDivide is not available
    temp_uint = static_cast<uint16_t>((tempVal * Gain) + offset);

    // Store in big-endian order: high byte first, then low byte
    DestinationStartAddress[0] = (temp_uint >> 8) & 0xFF;
    DestinationStartAddress[1] = temp_uint & 0xFF;
}
    
    } //end namespace serial

