#include "parser.h"

namespace PARSER{

Parser::Parser(QObject *parent) : QObject(parent)
{
    //ToDo why originally on the heap? parserTimer = new QElapsedTimer;
    //ToDo why originally on the heap?  parserClock = new QTime;
    parserTimer.start();
    latestTimeStamp_ms = 0; //setHMS(0, 0, 0, 0);
}

Parser::Parser(ParsedData parsedDataPointers, QObject *parent)
{
    if(parsedDataPointers.mutex != nullptr && parsedDataPointers.queueLabels != nullptr && parsedDataPointers.queueNumericData != nullptr
        && parsedDataPointers.queueTimeStamps != nullptr && parsedDataPointers.strqueuePrefiltered != nullptr)
    {
        parseddata = parsedDataPointers;
    }
    parserTimer.start();
    latestTimeStamp_ms = 0; //setHMS(0, 0, 0, 0);
}


void Parser::run()
{
    QScopedPointer<QEventLoop> parsloop(new QEventLoop);
    connect(this, &Parser::finished, parsloop.data(), &QEventLoop::quit);
    parsloop->exec();
}

// *** SO_B2F ***
float Parser::CanConvertBytesToFloat2(uint16_t* SourceStartAddres, float scaling, float offset) {
    volatile float tempVal;
    volatile uint16_t tempLowByte;
    volatile uint16_t tempHighByte;
    volatile uint16_t tempCompleteDataInt;

#ifdef CCS
    DINT;
#endif
// This reads the high byte first, followed by the low byte, which also assumes a big-endian byte order.
    tempHighByte = SourceStartAddres[0]; // safe, atomic data acquisition
    tempLowByte = SourceStartAddres[1]; // safe, atomic data acquisition
#ifdef CCS 
    EINT;
#endif

    tempCompleteDataInt = (tempHighByte << 8 | tempLowByte);

    tempVal = (tempCompleteDataInt * scaling - offset);

    return tempVal;
}
// *** EO_B2F ***

void Parser::parseSlot(QString inputString) {
    // qDebug() << "Parsing started...";

    listNumericData.clear();
    stringListLabels.clear();
    listTimeStamp.clear();
    lineCount = 0;

    QMap<int, QStringList> dataMap;  // Map to store data for each unique ID
    QMap<int, QVector<long>> timeMap;  // Map to store timestamps for each ID
    QList<QString> graphLabels;  // List with unique graph labels
    QList<QSharedPointer<QVector<QCPGraphData>>> chartGraphsData;  // List data for all graphs

    QStringList inputStringSplitArrayLines = inputString.split(QRegularExpression("[\\n\\r]+"), Qt::SkipEmptyParts);
    lineCount = inputStringSplitArrayLines.count();

    for (int l = 0; l < inputStringSplitArrayLines.count(); ++l) {
        parsingProgressPercent = (float)l / inputStringSplitArrayLines.count() * 100.0F;
        if (l % 50 == 0 && canReportProgress) {
            emit updateProgress(&parsingProgressPercent);
            QApplication::processEvents(); // Prevents app from freezing during processing large files
        }

        if (abortFlag) {
            abortFlag = false;
            break;
        }

        QString line = inputStringSplitArrayLines[l];
        // qDebug() << "Processing line: " << line;

        // Split the line into parts based on spaces
        QStringList parts = line.split(" ", Qt::SkipEmptyParts);

        if (parts.size() < 4) {
            qDebug() << "Invalid line format: " << line;
            continue;
        }

        // Parse timestamp
        bool timestampParsed = false;
        long timestamp = parts[0].toLong(&timestampParsed);
        if (!timestampParsed) {
            qDebug() << "Failed to parse timestamp from line: " << line;
            continue;  // Skip this line if the timestamp is not a valid integer
        }

        // Parse CAN ID
        bool frameIDParsed = false;
        int frameID = parts[1].toInt(&frameIDParsed);
        if (!frameIDParsed) {
            qDebug() << "Failed to parse frame ID from line: " << line;
            continue;  // Skip this line if the frame ID is not a valid integer
        }

        // Ensure data length part is in correct format
        if (!parts[2].startsWith("[") || !parts[2].endsWith("]")) {
            qDebug() << "Invalid data length format: " << parts[2];
            continue;
        }

        // Parse data length
        bool lengthParsed = false;
        int dataLength = parts[2].mid(1, parts[2].length() - 2).toInt(&lengthParsed);
        if (!lengthParsed) {
            qDebug() << "Failed to parse data length from line: " << line;
            continue;  // Skip this line if the data length is not valid
        }

        // Parse data bytes
        QStringList dataValues;
        for (int i = 3; i < 3 + dataLength && i < parts.size(); ++i) {
            dataValues.append(parts[i]);
        }

        if (dataValues.size() != dataLength) {
            qDebug() << "Data length mismatch in line: " << line;
            continue;
        }

        timeMap[frameID].append(timestamp);
        dataMap[frameID].append(dataValues.join(" "));  // Join data values as a single string

        if (!graphLabels.contains("ID " + QString::number(frameID))) {
            graphLabels.append("ID " + QString::number(frameID));
        }
    }

    // Debugging parsed data
    // qDebug() << "Parsed dataMap: " << dataMap;
    // qDebug() << "Parsed timeMap: " << timeMap;
    // qDebug() << "Parsed graphLabels: " << graphLabels;

    if (parseddata.mutex != nullptr) {
        while(parseddata.semaphorGUIprio->available() <= 0) {}

        QMutexLocker<QMutex> locker(parseddata.mutex.data());

        bool addToExistingQueueRecord = true;
        for (const QString &graphlabel : graphLabels) {
            if (parseddata.queueChartGraphLabels->indexOf(graphlabel) < 0) {
                addToExistingQueueRecord = false;
                break;
            }
        }

        if (addToExistingQueueRecord) {
            for (const QString &graphlabel : graphLabels) {
                QSharedPointer<QVector<QCPGraphData>> ptr_list_vec(chartGraphsData.at(graphLabels.lastIndexOf(graphlabel)));
                QSharedPointer<QVector<QCPGraphData>> ptr_queue_vec(parseddata.queueChartGraphs->at(parseddata.queueChartGraphLabels->lastIndexOf(graphlabel)));
                for (const QCPGraphData &vec : *ptr_list_vec) {
                    ptr_queue_vec->append(vec);
                }
            }
        } else {
            for (const QString &graphlabel : graphLabels) {
                parseddata.queueChartGraphLabels->enqueue(graphlabel);
            }
            for (const QSharedPointer<QVector<QCPGraphData>> &graphdata : chartGraphsData) {
                parseddata.queueChartGraphs->enqueue(graphdata);
            }
        }
        uint16_t elem1[2];
        uint16_t elem2[2];
        uint16_t elem3[2];
        uint16_t elem4[2];

        // Update old-style lists *** @Develop ***
        for (const int frameID : dataMap.keys()) {
            for (const QString &value : dataMap[frameID]) {
                uint16_t elem1[2] = {0, 0};
                uint16_t elem2[2] = {0, 0};
                uint16_t elem3[2] = {0, 0};
                uint16_t elem4[2] = {0, 0};
                QStringList dataParts = value.split(" ");

                // Ensure we have enough data parts
                if (dataParts.size() < 8) {
                    qDebug() << "Not enough data parts in: " << value;
                    continue;
                }

                elem1[0] = dataParts[0].toUInt(nullptr, 16);
                elem1[1] = dataParts[1].toUInt(nullptr, 16);
                elem2[0] = dataParts[2].toUInt(nullptr, 16);
                elem2[1] = dataParts[3].toUInt(nullptr, 16);
                elem3[0] = dataParts[4].toUInt(nullptr, 16);
                elem3[1] = dataParts[5].toUInt(nullptr, 16);
                elem4[0] = dataParts[6].toUInt(nullptr, 16);
                elem4[1] = dataParts[7].toUInt(nullptr, 16);

                // Debugging elements
                // qDebug() << "Elem1: " << elem1[0] << elem1[1];
                // qDebug() << "Elem2: " << elem2[0] << elem2[1];
                // qDebug() << "Elem3: " << elem3[0] << elem3[1];
                // qDebug() << "Elem4: " << elem4[0] << elem4[1];

                if (frameID == 3) {
                    listNumericData.append(CanConvertBytesToFloat2(elem1, 0.0000762939453125, 0.0));
                    listNumericData.append(CanConvertBytesToFloat2(elem2, 0.0000762939453125, 0.0));
                    listNumericData.append(CanConvertBytesToFloat2(elem3, 0.0000762939453125, 0.0));
                    listNumericData.append(CanConvertBytesToFloat2(elem4, 0.0000762939453125, 0.0));
                    stringListLabels.append("Phase_1_[A]");
                    stringListLabels.append("Phase_2_[A]");
                    stringListLabels.append("Phase_3_[A]");
                    stringListLabels.append("Phase_4_[A]");
                }

                if (frameID == 4) {
                    listNumericData.append(CanConvertBytesToFloat2(elem1, 0.0000762939453125, 0.0));
                    listNumericData.append(CanConvertBytesToFloat2(elem2, 0.0000762939453125, 0.0));
                    listNumericData.append(CanConvertBytesToFloat2(elem3, 0.000915527, 0.0));
                    listNumericData.append(CanConvertBytesToFloat2(elem4, 0.000915527, 0.0));
                    stringListLabels.append("Phase_5_[A]");
                    stringListLabels.append("Phase_6_[A]");
                    stringListLabels.append("Uin_[V]");
                    stringListLabels.append("Uout_[V]");
                }

                listTimeStamp.append(timeMap[frameID].last());
                listTimeStamp.append(timeMap[frameID].last());
                listTimeStamp.append(timeMap[frameID].last());
                listTimeStamp.append(timeMap[frameID].last());
            }
        }

        for (const QString &label : stringListLabels) {
            parseddata.queueLabels->enqueue(label);
        }
        for (const double &record : listNumericData) {
            parseddata.queueNumericData->enqueue(record);
        }
        for (const long &tstamp : listTimeStamp) {
            parseddata.queueTimeStamps->enqueue(tstamp);
        }
        if (parsSettings.prefilterMode == None) {
            parseddata.strqueuePrefiltered->append(inputString);
        } else if (parsSettings.prefilterMode == simplified) {
            parseddata.strqueuePrefiltered->append(inputString.simplified());
        } else if (parsSettings.prefilterMode == trimmed) {
            parseddata.strqueuePrefiltered->append(inputString.trimmed());
        }

        // Debugging parsed data before updating GUI
        // qDebug() << "Queue Chart Graph Labels: " << *parseddata.queueChartGraphLabels;
        // qDebug() << "Queue Numeric Data: " << *parseddata.queueNumericData;
        // qDebug() << "Queue Time Stamps: " << *parseddata.queueTimeStamps;
        // qDebug() << "Queue Labels: " << *parseddata.queueLabels;
    }
}

void Parser::finish()
{
    emit finished();
}

void Parser::parse(QString inputString, QString externalClockLabel, serial::SERIAL_TSTAMP_MODE tstampmode,
                   int fixinterval, double timebase_s)
{
    // Initialize parsing settings
    parsSettings.timestampMode = tstampmode;
    parsSettings.externalClockLabel = externalClockLabel;
    parsSettings.fixinterval = fixinterval;
    parsSettings.timebase_s = timebase_s;

    // Old style
    listNumericData.clear();
    stringListLabels.clear();
    listTimeStamp.clear();
    lineCount = 0;

    // New style
    QMap<int, QStringList> dataMap;  // Map to store data for each unique ID
    QMap<int, long> timeMap;  // Map to store the latest timestamp for each ID
    QList<QString> graphLabels;  // List with unique graph labels
    QList<QSharedPointer<QVector<QCPGraphData>>> chartGraphsData;  // List data for all graphs

    QStringList inputStringSplitArrayLines = inputString.split(QRegularExpression("[\\n+\\r+]"), Qt::SplitBehaviorFlags::SkipEmptyParts);
    lineCount = inputStringSplitArrayLines.count();

    QRegularExpression mainSymbols("^[-+]?[0-9]*\\.?[0-9]+$"); // Declare mainSymbols
    QRegularExpression sepSymbols("[\t=,;]"); // Declare sepSymbols

    for (auto l = 0; l < inputStringSplitArrayLines.count(); ++l)
    {
        parsingProgressPercent = (float)l / inputStringSplitArrayLines.count() * 100.0F;
        if (l % 50 == 0 && canReportProgress)
        {
            emit updateProgress(&parsingProgressPercent);
            QApplication::processEvents(); // Prevents app from freezing during processing large files
        }

        if (abortFlag)
        {
            abortFlag = false;
            break;
        }

        inputStringSplitArrayLines[l].replace(sepSymbols, " ");
        QStringList inputStringSplitArray = inputStringSplitArrayLines[l].simplified().split(QRegularExpression("\\s+"), Qt::SplitBehaviorFlags::SkipEmptyParts);

        if (inputStringSplitArray.size() < 2) continue;  // Skip if line doesn't have enough data

        int frameID = inputStringSplitArray[0].toInt();
        inputStringSplitArray.removeFirst();  // Remove ID from the array

        if (parsSettings.timestampMode == serial::SysTimeStamp && inputStringSplitArray.last().contains("RDTSTAMP"))
        {
            long tmpms = inputStringSplitArray.last().replace("RDTSTAMP", "").toInt();
            QTime tmpTime = QTime::fromMSecsSinceStartOfDay(tmpms);
            if(tmpTime.isValid())
            {
                timeMap[frameID] = tmpms;
            }
            else
            {
                timeMap[frameID] += parsSettings.fixinterval;
                if(timeMap[frameID] > MS_PER_DAY) { timeMap[frameID] -= MS_PER_DAY; }
            }
            inputStringSplitArray.removeLast();
        }
        else if (parsSettings.timestampMode == serial::FixIntervalTStamp)
        {
            timeMap[frameID] += parsSettings.fixinterval;
            if(timeMap[frameID] > MS_PER_DAY) { timeMap[frameID] -= MS_PER_DAY; }
        }
        else
        {
            listTimeStamp.append(parserTimer.elapsed());
        }

        for (auto i = 0; i < inputStringSplitArray.count(); ++i)
        {
            if (mainSymbols.match(inputStringSplitArray[i]).hasMatch())
            {
                dataMap[frameID].append(inputStringSplitArray[i]);
            }
        }

        for (const auto& frameID : dataMap.keys())
        {
            QString tmplabel = "ID " + QString::number(frameID);
            QCPGraphData tmpdata_with_time;
            tmpdata_with_time.key = timeMap[frameID] * parsSettings.timebase_s;
            tmpdata_with_time.value = dataMap[frameID].join(" ").toDouble();
            int tmplabelindex = graphLabels.indexOf(tmplabel);
            if(tmplabelindex < 0)
            {
                graphLabels.append(tmplabel);
                QSharedPointer<QVector<QCPGraphData>> tmpshardptr(new QVector<QCPGraphData>());
                tmpshardptr->append(tmpdata_with_time);
                chartGraphsData.append(tmpshardptr);
            }
            else
            {
                chartGraphsData[tmplabelindex]->append(tmpdata_with_time);
            }
        }
    }
}


void Parser::parseCSV(QString inputString, bool useExternalLabel, QString externalClockLabel)
{
    listNumericData.clear();
    stringListLabels.clear();
    listTimeStamp.clear();
    lineCount = 0;

    QStringList inputStringSplitArrayLines = inputString.split(QRegularExpression("[\\n+\\r+]"), Qt::SplitBehaviorFlags::SkipEmptyParts);
    lineCount = inputStringSplitArrayLines.count();

    QStringList csvLabels; // !

    QRegularExpression mainSymbols("^[-+]?[0-9]*\\.?[0-9]+$"); // Declare mainSymbols
    QRegularExpression sepSymbols("[=,]"); // Declare sepSymbols

    for (auto l = 0; l < lineCount; ++l)
    {
        parsingProgressPercent = (float)l / lineCount * 100.0F;
        if (l % 50 == 0 && canReportProgress)
        {
            emit updateProgress(&parsingProgressPercent);
            QApplication::processEvents(); // Prevents app from freezing during processing large files
        }

        if (abortFlag)
        {
            abortFlag = false;
            break;
        }

        inputStringSplitArrayLines[l].replace(sepSymbols, " ");
        inputStringSplitArrayLines[l].remove("\"");

        QStringList inputStringSplitArray = inputStringSplitArrayLines[l].simplified().split(QRegularExpression("\\s+"), Qt::SplitBehaviorFlags::SkipEmptyParts);

        // Look for labels
        if (l == 0)
        {
            for (auto i = 0; i < inputStringSplitArray.count(); ++i)
            {
                if (!sepSymbols.match(inputStringSplitArray[i]).hasMatch())
                {
                    if (!csvLabels.contains(inputStringSplitArray[i]))
                        csvLabels.append(inputStringSplitArray[i]);
                }
            }
        }

        // Look for time reference
        for (auto i = 0; i < inputStringSplitArray.count(); ++i)
        {
            if (useExternalLabel == true && externalClockLabel.isEmpty() == false && i == csvLabels.indexOf(externalClockLabel))
            {
                latestTimeStamp_ms = inputStringSplitArray[i].toLong();
                break;
            }
            else if (useExternalLabel == false)
            {
                foreach (auto timeFormat, searchTimeFormatList)
                {
                    if (QTime::fromString(inputStringSplitArray[i], timeFormat).isValid())
                    {
                        latestTimeStamp_ms = this->timeTomsSinceBeginOfDay(QTime::fromString(inputStringSplitArray[i], timeFormat));

                        if (minimumTime != QTime(0, 0, 0) && maximumTime != QTime(0, 0, 0))
                        {
                            if (QTime::fromMSecsSinceStartOfDay(latestTimeStamp_ms) < minimumTime
                                || QTime::fromMSecsSinceStartOfDay(latestTimeStamp_ms) > maximumTime)
                            {
                                continue;
                            }
                        }
                        break;
                    }
                }
            }
        }

        // Look for data
        for (auto i = 0; i < inputStringSplitArray.count(); ++i)
        {
            if (mainSymbols.match(inputStringSplitArray[i]).hasMatch())
            {
                if (i >= csvLabels.count())
                    continue; // TODO ERROR REPORTING
                stringListLabels.append(csvLabels[i]);
                listNumericData.append(inputStringSplitArray[i].toDouble());
                listTimeStamp.append(latestTimeStamp_ms);
            }
        }
    }
}

void Parser::getCSVReadyData(QStringList *columnNames, QList<QList<double>> *dataColumns)
{
    QStringList labelStorage = this->getLabelStorage();
    QStringList tempColumnNames = labelStorage;
    tempColumnNames.removeDuplicates();
    QList<QList<double>> tempColumnsData;
    QList<double> numericDataList = this->getDataStorage();

    for (auto i = 0; i < tempColumnNames.count(); ++i)
    {
        tempColumnsData.append(*new QList<double>);

        while (labelStorage.contains(tempColumnNames[i]))
        {
            tempColumnsData[tempColumnsData.count() - 1].append(numericDataList.takeAt(labelStorage.indexOf(tempColumnNames[i])));
            labelStorage.removeAt(labelStorage.indexOf(tempColumnNames[i]));
        }
    }

    *columnNames = tempColumnNames;
    *dataColumns = tempColumnsData;
}

long Parser::timeTomsSinceBeginOfDay(QTime time)
{
    return MS_PER_DAY - long(time.msecsTo(QTime(0,0,0,0)));
}

void Parser::setParseSettings(int tstampMode, QString extClockLabel, int fixintv, double tbase_s, int prefltr)
{
    if(tstampMode > -1 && tstampMode <= 3)
    {
        parsSettings.timestampMode = static_cast<serial::SERIAL_TSTAMP_MODE>(tstampMode);
    }
    if(extClockLabel != "")
    {
        parsSettings.externalClockLabel = extClockLabel;
    }
    if(fixintv > 0)
    {
        parsSettings.fixinterval = fixintv;
    }
    if(tbase_s > 0)
    {
        parsSettings.timebase_s = tbase_s;
    }
    if(prefltr > -1 && prefltr <= 3)
    {
        parsSettings.prefilterMode = static_cast<PARSER_PRE_FILTER_MODE>(prefltr);
    }
}


QList<double> Parser::getDataStorage() { return dataStorage; }
QList<double> Parser::getListNumericValues() { return listNumericData; }
QList<long> Parser::getListTimeStamp() { return listTimeStamp; }
QList<long> Parser::getTimeStorage() { return timeStampStorage; }
QStringList Parser::getLabelStorage() { return labelStorage; }
QStringList Parser::getStringListLabels() { return stringListLabels; }
QStringList Parser::getStringListNumericData() { return stringListNumericData; }
QStringList Parser::getTextList() { return textStorage; }
void Parser::clearExternalClock() { latestTimeStamp_ms = 0;} //.setHMS(0, 0, 0, 0);

void Parser::restartChartTimer()
{
    parserTimer.restart();

}

void Parser::parserClockAddMSecs(int ms)
{
    parserClock = parserClock.addMSecs(ms);
    // parserTimer->start();
}

void Parser::appendSetToMemory(QStringList newlabelList, QList<double> newDataList, QList<long> newTimeList, QString text)
{
    labelStorage.append(newlabelList);
    dataStorage.append(newDataList);
    timeStampStorage.append(newTimeList);

    if (!text.isEmpty())
        textStorage.append(text);
}

void Parser::clearStorage()
{
    labelStorage.clear();
    dataStorage.clear();
    timeStampStorage.clear();
    textStorage.clear();
}

void Parser::clear()
{
    stringListLabels.clear();
    stringListNumericData.clear();
    listTimeStamp.clear();
}

void Parser::setParsingTimeRange(QTime minTime, QTime maxTime)  //ToDo to analyze
{
    minimumTime = minTime;
    maximumTime = maxTime;
}

void Parser::resetTimeRange()
{
    minimumTime.setHMS(0, 0, 0);
    maximumTime.setHMS(0, 0, 0);
}

void Parser::abort() { abortFlag = true; }
void Parser::setReportProgress(bool isEnabled) { canReportProgress = isEnabled; }

}//end namespace
