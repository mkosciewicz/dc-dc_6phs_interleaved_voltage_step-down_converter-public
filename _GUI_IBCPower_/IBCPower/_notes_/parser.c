#ifndef PARSER_H
#define PARSER_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <pthread.h>
#include <semaphore.h>
#include <time.h>

#define MS_PER_DAY 86400000L

typedef enum {
    None = 0,
    trimmed,
    simplified
} PARSER_PRE_FILTER_MODE;

typedef struct {
    pthread_mutex_t *mutex;
    sem_t *semaphorGUIprio;
    char **queueLabels;
    double *queueNumericData;
    long *queueTimeStamps;
    char *strqueuePrefiltered;
    char **queueChartGraphLabels;
    double **queueChartGraphs;
} ParsedData;

typedef struct {
    bool abortFlag;
    bool canReportProgress;
    float parsingProgressPercent;
    int lineCount;
    struct {
        int timestampMode;
        char *externalClockLabel;
        int fixinterval;
        double timebase_s;
        PARSER_PRE_FILTER_MODE prefilterMode;
    } parsSettings;
    ParsedData parseddata;
    double *dataStorage;
    double *listNumericData;
    long *listTimeStamp;
    long *timeStampStorage;
    char **labelStorage;
    char **stringListNumericData;
    char **stringListLabels;
    char **textStorage;
    clock_t parserTimer;
    struct tm parserClock;
    long latestTimeStamp_ms;
    struct tm minimumTime;
    struct tm maximumTime;
    pthread_mutex_t mutex;
} Parser;

void Parser_init(Parser *parser) {
    parser->abortFlag = false;
    parser->canReportProgress = false;
    parser->parsingProgressPercent = 0.0f;
    parser->lineCount = 0;
    parser->parsSettings.timestampMode = 0; // serial::NoTStamp
    parser->parsSettings.externalClockLabel = NULL;
    parser->parsSettings.fixinterval = 2;
    parser->parsSettings.timebase_s = 0.001;
    parser->parsSettings.prefilterMode = None;
    parser->parseddata.mutex = NULL;
    parser->parseddata.semaphorGUIprio = NULL;
    parser->parseddata.queueLabels = NULL;
    parser->parseddata.queueNumericData = NULL;
    parser->parseddata.queueTimeStamps = NULL;
    parser->parseddata.strqueuePrefiltered = NULL;
    parser->parseddata.queueChartGraphLabels = NULL;
    parser->parseddata.queueChartGraphs = NULL;
    parser->dataStorage = NULL;
    parser->listNumericData = NULL;
    parser->listTimeStamp = NULL;
    parser->timeStampStorage = NULL;
    parser->labelStorage = NULL;
    parser->stringListNumericData = NULL;
    parser->stringListLabels = NULL;
    parser->textStorage = NULL;
    parser->parserTimer = clock();
    parser->latestTimeStamp_ms = 0;
    memset(&parser->minimumTime, 0, sizeof(struct tm));
    memset(&parser->maximumTime, 0, sizeof(struct tm));
    pthread_mutex_init(&parser->mutex, NULL);
}

void Parser_parse(Parser *parser, char *inputString) {
    // Example logic for processing inputString
    char *line = strtok(inputString, "\n");
    while (line != NULL) {
        // Process each line
        // Update progress
        line = strtok(NULL, "\n");
    }
}

void Parser_clear(Parser *parser) {
    // Clear the parser state
    free(parser->listNumericData);
    free(parser->stringListLabels);
    free(parser->listTimeStamp);
    parser->listNumericData = NULL;
    parser->stringListLabels = NULL;
    parser->listTimeStamp = NULL;
}

void Parser_setParsingTimeRange(Parser *parser, struct tm minTime, struct tm maxTime) {
    parser->minimumTime = minTime;
    parser->maximumTime = maxTime;
}

void Parser_resetTimeRange(Parser *parser) {
    memset(&parser->minimumTime, 0, sizeof(struct tm));
    memset(&parser->maximumTime, 0, sizeof(struct tm));
}

void Parser_abort(Parser *parser) {
    parser->abortFlag = true;
}

void Parser_setReportProgress(Parser *parser, bool isEnabled) {
    parser->canReportProgress = isEnabled;
}

long timeTomsSinceBeginOfDay(struct tm time) {
    return MS_PER_DAY - (long) ((time.tm_hour * 3600 + time.tm_min * 60 + time.tm_sec) * 1000);
}

void Parser_appendSetToMemory(Parser *parser, char **newlabelList, double *newDataList, long *newTimeList, int newSize, char *text) {
    // Append new data to storage
    // Assuming memory is managed properly elsewhere
}

void Parser_clearStorage(Parser *parser) {
    // Clear storage
    free(parser->labelStorage);
    free(parser->dataStorage);
    free(parser->timeStampStorage);
    free(parser->textStorage);
    parser->labelStorage = NULL;
    parser->dataStorage = NULL;
    parser->timeStampStorage = NULL;
    parser->textStorage = NULL;
}

void Parser_restartChartTimer(Parser *parser) {
    parser->parserTimer = clock();
}

void Parser_parserClockAddMSecs(Parser *parser, int ms) {
    // Adding milliseconds to parserClock
    time_t rawtime = mktime(&parser->parserClock);
    rawtime += ms / 1000;
    parser->parserClock = *localtime(&rawtime);
}

#endif // PARSER_H
