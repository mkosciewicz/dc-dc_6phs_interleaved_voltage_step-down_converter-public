#ifndef SERIAL_H
#define SERIAL_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <pthread.h>
#include <semaphore.h>
#include <time.h>
#include <unistd.h>

#define MS_PER_DAY 86400000L

typedef enum {
    canReadLine_ReadLine = 0,
    canReadLine_ReadAll,
    bytesAvailable_ReadLine,
    bytesAvailable_ReadAll
} SERIAL_READ_MODE;

typedef enum {
    SysTimeStamp = 0,
    ExternalTStamp,
    NoTStamp,
    FixIntervalTStamp,
} SERIAL_TSTAMP_MODE;

typedef struct {
    bool abortFlag;
    bool canReportProgress;
    float parsingProgressPercent;
    int lineCount;
    SERIAL_READ_MODE stringReadMode;
    SERIAL_TSTAMP_MODE timestampMode;
    char *serialInputString;
    long latestTimeStamp_ms;
    struct timespec parserTimer;
    struct tm parserClock;
    pthread_mutex_t mutex;
    sem_t semaphorGUIprio;
    int serialFd; // File descriptor for the serial port
} Serial;

void Serial_init(Serial *serial) {
    serial->abortFlag = false;
    serial->canReportProgress = false;
    serial->parsingProgressPercent = 0.0f;
    serial->lineCount = 0;
    serial->stringReadMode = canReadLine_ReadLine;
    serial->timestampMode = NoTStamp;
    serial->serialInputString = (char *)malloc(1024 * sizeof(char));
    serial->latestTimeStamp_ms = 0;
    clock_gettime(CLOCK_REALTIME, &serial->parserTimer);
    memset(&serial->parserClock, 0, sizeof(struct tm));
    pthread_mutex_init(&serial->mutex, NULL);
    sem_init(&serial->semaphorGUIprio, 0, 1);
    serial->serialFd = -1; // Invalid file descriptor initially
}

void Serial_addTstampString(Serial *serial, char *targetString) {
    char timestamp[50];
    if (serial->timestampMode == SysTimeStamp) {
        struct timespec now;
        clock_gettime(CLOCK_REALTIME, &now);
        long ms_since_start_of_day = (now.tv_sec % (24 * 3600)) * 1000 + now.tv_nsec / 1000000;
        snprintf(timestamp, sizeof(timestamp), "RDTSTAMP%ld", ms_since_start_of_day);
        strcat(targetString, timestamp);
    }
}

void Serial_readString(Serial *serial) {
    char buffer[1024];
    ssize_t bytesRead;
    while ((bytesRead = read(serial->serialFd, buffer, sizeof(buffer) - 1)) > 0) {
        buffer[bytesRead] = '\0';
        strcat(serial->serialInputString, buffer);
        Serial_addTstampString(serial, serial->serialInputString);
    }
}

bool Serial_setReadMode(Serial *serial, int mode) {
    switch (mode) {
        case 0:
            serial->stringReadMode = canReadLine_ReadLine;
            break;
        case 1:
            serial->stringReadMode = canReadLine_ReadAll;
            break;
        case 2:
            serial->stringReadMode = bytesAvailable_ReadLine;
            break;
        case 3:
            serial->stringReadMode = bytesAvailable_ReadAll;
            break;
        default:
            return false;
    }
    return true;
}

bool Serial_setTimestampMode(Serial *serial, int mode) {
    switch (mode) {
        case 0:
            serial->timestampMode = SysTimeStamp;
            break;
        case 1:
            serial->timestampMode = ExternalTStamp;
            break;
        case 2:
            serial->timestampMode = NoTStamp;
            break;
        case 3:
            serial->timestampMode = FixIntervalTStamp;
            break;
        default:
            serial->timestampMode = NoTStamp;
            return false;
    }
    return true;
}

char* Serial_getString(Serial *serial, bool clearBuffer) {
    char *output = strdup(serial->serialInputString);
    if (clearBuffer) {
        Serial_clearString(serial);
    }
    return output;
}

void Serial_clearAll(Serial *serial, bool clearHardwareBuffers) {
    serial->serialInputString[0] = '\0';
    if (clearHardwareBuffers) {
        tcflush(serial->serialFd, TCIOFLUSH);
    }
}

void Serial_clearString(Serial *serial) {
    serial->serialInputString[0] = '\0';
}

bool Serial_isOpen(Serial *serial) {
    return serial->serialFd >= 0;
}

bool Serial_begin(Serial *serial, const char *portName, int baudRate) {
    serial->serialFd = open(portName, O_RDWR | O_NOCTTY | O_NDELAY);
    if (serial->serialFd == -1) {
        perror("Unable to open port");
        return false;
    }
    struct termios options;
    tcgetattr(serial->serialFd, &options);
    cfsetispeed(&options, baudRate);
    cfsetospeed(&options, baudRate);
    options.c_cflag |= (CLOCAL | CREAD);
    tcsetattr(serial->serialFd, TCSANOW, &options);
    return true;
}

bool Serial_end(Serial *serial) {
    if (serial->serialFd != -1) {
        close(serial->serialFd);
        serial->serialFd = -1;
    }
    return true;
}

bool Serial_send(Serial *serial, const char *message) {
    if (serial->serialFd != -1) {
        write(serial->serialFd, message, strlen(message));
        return true;
    }
    return false;
}

void Serial_run(Serial *serial) {
    while (!serial->abortFlag) {
        Serial_readString(serial);
        usleep(100000); // Sleep for 100ms
    }
}

#endif // SERIAL_H
