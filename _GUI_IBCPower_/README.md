# IBCPower - Release Notes

## First Compilation

Po zrobieniu **```git pull```** pojawił się błąd jak na poniższym obrazku:
| QtCreator |
|-------|
| ![Pierwsa kompilacja](_ReleaseNotes_/images/1st_run.png) |
| Błąd spowodowany umieszczeniem plików **.h** do folderu 'headers' -> Potrzeba ręcznie dodać|

|Przyczyna|
|---------|
| ![Pliki Phantom](_ReleaseNotes_/images/pliki_phantom.png)| 
|Wyblakłe pliki to są te ktore się tworzą przy kompilacji. Dlatego jest ustawiony '.gitinore' co pozwala żeby one zostały tylko na lokalnym PC. Z tego powodu jest potrzeba dopisać "headers/*.h"|

## Version 0.0.1

## Version 0.0.2

### New Features

- Feature 1: Description of the new feature.
- Feature 2: Description of the new feature.

### Enhancements

- Enhancement 1: Description of the enhancement.
- Enhancement 2: Description of the enhancement.

### Bug Fixes

- Bug Fix 1: Description of the bug fix.
- Bug Fix 2: Description of the bug fix.

### Known Issues

- Issue 1: Description of the known issue.
- Issue 2: Description of the known issue.
