#include "F28x_Project.h"

#include "hdrs/initReglsPI.h"
#include "hdrs/reglsPI.h"
#include "hdrs/setupISR.h"
#include "hdrs/setupADC.h"
#include "hdrs/setupCAN.h"
#include "hdrs/setupGPIO.h"
#include "hdrs/setupPWM.h"
#include "hdrs/setupTIMER.h"
#include "hdrs/dataCAN.h"
#include "hdrs/enableISR.h"

unsigned int mainLoopCounter = 0;

int main(void) {
    InitSysCtrl();  // Initialize system control

//    /* SO 4 homeOffice debug */
//    dataADC.phs_1_curr = 1.0;
//    dataADC.phs_2_curr = 1.5;
//    dataADC.phs_3_curr = 2.0;
//    dataADC.phs_4_curr = 2.5;
//    dataADC.phs_5_curr = 3.0;
//    dataADC.phs_6_curr = 3.5;
//    dataADC.Uin_vltg = 50.0;
//    dataADC.Uout_vltg = 40.5;
//    /* EO 4 homeOffice debug */

    DINT;
    InitPieCtrl();  // Initialize the PIE control registers
    IER = 0x0000;
    IFR = 0x0000;

    EALLOW;
    CpuSysRegs.PCLKCR0.bit.CPUTIMER0 = 1;
    EDIS;

    InitPieVectTable();

    // Setup ISRs in the PIE vector table
    setupISR();

    // Initialize peripherals
    setupGPIO();
    setupTIMER();
    setupPWM();
    setupADC();

    // Initialize PI controllers
    initReglsPI();

    // Initialize CAN network
    setupCAN();
    mailboxingCAN();

    // Exit CAN initialization mode
    CanbRegs.CAN_CTL.bit.Init = 0;

    // Enable global interrupts
    enableISR();

    // Main control loop
    while (1) {
        mainLoopCounter++;
        handleCANTx();
        handleCANRx();
    }
}
