# TMS320F2837xD

## Project Status: 16th April 2024

### **Closed Loop Epic:** Completed

The issue of falling into DCM has been resolved by adding a 2.2 [Ω] resistor, which provides a baseline to prevent I=0[A] situations.

### Still_2BDone:

 - Complete Measurement Report

 - Warning in Test Plan regarding discontinuities. In further development, an algorithm needs to be implemented to control the number of activated branches based on whether we enter DCM.
