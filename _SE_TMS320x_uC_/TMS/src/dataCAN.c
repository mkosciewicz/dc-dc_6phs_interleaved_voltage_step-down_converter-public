#include "F28x_Project.h"
#include "hdrs/setupCAN.h"
#include "hdrs/dataCAN.h"
#include "hdrs/setupPWM.h"
#include <string.h>

// ========== SO_Defines =============
#define UREF_Rx_SCALE_FACTOR 0.000915527 // maxVoltage 60 [V]
#define Voltage_Tx_SCALE_FACTOR  1092.26666666667 // maxVoltage 60 [V]
#define Currents_Tx_SCALE_FACTOR 13107.2 // maxCurrent 5 [A]
#define OFFSET 0x00

extern volatile bool can_tx_flag, can_rx_flag;

struct CAN_TX_MESSAGE_STRUCT txMessage1;
struct CAN_TX_MESSAGE_STRUCT txMessage2;

struct CAN_RX_MESSAGE_STRUCT rxMessage;
// struct CAN_RX_MESSAGE_STRUCT rxMessage2;

// Define the function pointers for control actions
typedef void (*CtrlActionFunc)(void);

CtrlActionFunc bitsOnFunctions[] = {ProtectionON, PWM1_ON, PWM2_ON, PWM3_ON, PWM4_ON, PWM5_ON, PWM6_ON, ResetON};
CtrlActionFunc bitsOffFunctions[] = {ProtectionOFF, PWM1_OFF, PWM2_OFF, PWM3_OFF, PWM4_OFF, PWM5_OFF, PWM6_OFF, ResetOFF};
// ========== EO_Defines =============

// ========== SO_Convs ============
// *** F2B ***
void CanConvertFloatToBytes2(uint16_t *DestinationStartAddress, volatile float *inputData, float scaling, float offset) {
	volatile float tempVal, Gain;
	volatile uint16_t temp_uint;

#ifdef CCS
	DINT;  // Disable interrupts
#endif
	tempVal = *inputData; // Safe, atomic data acquisition
    Gain = scaling;
#ifdef CCS
	EINT;  // Enable interrupts
#endif

	// Replace _fastFloatDivide with direct division if _fastFloatDivide is not available
	temp_uint = (uint16_t)(((tempVal*Gain) + offset));

    //Here, the high byte of temp_uint is stored first, followed by the low byte, which indicates big-endian byte order.
    DestinationStartAddress[0] = (temp_uint & 0xFF00) >> 8;
	DestinationStartAddress[1] = (temp_uint & 0x00FF);
	
}

// *** B2F ***
float CanConvertBytesToFloat2(uint16_t *SourceStartAddres, float scaling, float offset) {
	volatile float      tempVal;
	volatile uint16_t   tempLowByte;
	volatile uint16_t   tempHighByte;
	volatile uint16_t   tempCompleteDataInt;

#ifdef CCS
	DINT;
#endif
	tempHighByte = SourceStartAddres[0]; // safe, atomic data aquisition
    tempLowByte = SourceStartAddres[1]; // safe, atomic data aquisition
#ifdef CCS 
	EINT;
#endif

	tempCompleteDataInt = (tempHighByte << 8 | tempLowByte);

	tempVal = (tempCompleteDataInt * scaling - offset);

	return tempVal;
}
// ========== EO_Convs ============

// ========== SO_Functions =============
int dataCANTx(struct CAN_TX_MESSAGE_STRUCT *msg, uint32_t CanID) {
    msg->CanID = CanID;
    memset(msg->data, OFFSET, sizeof(msg->data));  // Clearing the data array

    switch (CanID) {
//			case 0x03:
//				CanConvertFloatToBytes2((uint16_t*)&msg->data[0], &dataADC.phs_1_curr, Currents_Tx_SCALE_FACTOR, OFFSET);
//				CanConvertFloatToBytes2((uint16_t*)&msg->data[2], &dataADC.phs_2_curr, Currents_Tx_SCALE_FACTOR, OFFSET);
//				CanConvertFloatToBytes2((uint16_t*)&msg->data[4], &dataADC.phs_3_curr, Currents_Tx_SCALE_FACTOR, OFFSET);
//				CanConvertFloatToBytes2((uint16_t*)&msg->data[6], &dataADC.phs_4_curr, Currents_Tx_SCALE_FACTOR, OFFSET);
//				break;
//			case 0x04:
//				CanConvertFloatToBytes2((uint16_t*)&msg->data[0], &dataADC.phs_5_curr, Currents_Tx_SCALE_FACTOR, OFFSET);
//				CanConvertFloatToBytes2((uint16_t*)&msg->data[2], &dataADC.phs_6_curr, Currents_Tx_SCALE_FACTOR, OFFSET);
//				CanConvertFloatToBytes2((uint16_t*)&msg->data[4], &dataADC.Uin_vltg, Voltage_Tx_SCALE_FACTOR, OFFSET);
//				CanConvertFloatToBytes2((uint16_t*)&msg->data[6], &dataADC.Uout_vltg, Voltage_Tx_SCALE_FACTOR, OFFSET);

         case 0x03:
             CanConvertFloatToBytes2((uint16_t*)&msg->data[0], &dataADC.Current_1, Currents_Tx_SCALE_FACTOR, OFFSET);
             CanConvertFloatToBytes2((uint16_t*)&msg->data[2], &dataADC.Current_2, Currents_Tx_SCALE_FACTOR, OFFSET);
             CanConvertFloatToBytes2((uint16_t*)&msg->data[4], &dataADC.Current_3, Currents_Tx_SCALE_FACTOR, OFFSET);
             CanConvertFloatToBytes2((uint16_t*)&msg->data[6], &dataADC.Current_4, Currents_Tx_SCALE_FACTOR, OFFSET);
             break;
         case 0x04:
             CanConvertFloatToBytes2((uint16_t*)&msg->data[0], &dataADC.Current_5, Currents_Tx_SCALE_FACTOR, OFFSET);
             CanConvertFloatToBytes2((uint16_t*)&msg->data[2], &dataADC.Current_6, Currents_Tx_SCALE_FACTOR, OFFSET);
             CanConvertFloatToBytes2((uint16_t*)&msg->data[4], &dataADC.Voltage_in, Voltage_Tx_SCALE_FACTOR, OFFSET);
             CanConvertFloatToBytes2((uint16_t*)&msg->data[6], &dataADC.Voltage_out, Voltage_Tx_SCALE_FACTOR, OFFSET);

            break;
        default:
            return -1; // Handle unknown CAN ID error
    }
    return 0;
}

float dataCANRx(struct CAN_RX_MESSAGE_STRUCT *msg, uint32_t CanID) {
    CAN_MessageRead(msg);
    msg->CanID = CanID;
    uint16_t byte0 = msg->data[0];
    uint16_t byte4 = msg->data[4];

    if (!msg->newDataAvailable) {
        return -1; // No new data available, exit the function
    }

    switch (CanID) {
        case 0x07:
            if (byte0 & 0x01) {
                bitsOnFunctions[1]();
            } else {
                bitsOffFunctions[1]();
            }
            if (byte0 & 0x02) {
                bitsOnFunctions[2]();
            } else {
                bitsOffFunctions[2]();
            }
            if (byte0 & 0x04) {
                bitsOnFunctions[3]();
            } else {
                bitsOffFunctions[3]();
            }
            if (byte0 & 0x08) {
                bitsOnFunctions[4]();
            } else {
                bitsOffFunctions[4]();
            }
            if (byte0 & 0x10) {
                bitsOnFunctions[5]();
            } else {
                bitsOffFunctions[5]();
            }
            if (byte0 & 0x20) {
                bitsOnFunctions[6]();
            } else {
                bitsOffFunctions[6]();
            }

            dataADC.U_REF = CanConvertBytesToFloat2((uint16_t*)&msg->data[2], UREF_Rx_SCALE_FACTOR, OFFSET);

            if (byte4 & 0x01) {
                bitsOnFunctions[0]();
            } else {
                bitsOffFunctions[0]();
            }
            if (byte4 & 0x02) {
                bitsOnFunctions[7]();
            } else {
                bitsOffFunctions[7]();
            }
            break;

        default:
            break;
    }

    msg->newDataAvailable = 0;
    return 0;
}

void mailboxingCAN(){
    CAN_MailBoxTxSetAndInit(&txMessage1, 13, CAN_11BIT, 0x03, 8);
    CAN_MailBoxTxSetAndInit(&txMessage2, 14, CAN_11BIT, 0x04, 8);

    CAN_MailBoxRxSetAndInit(&rxMessage, 2, CAN_11BIT, 0x07, 8, 0x7FF);
    // CAN_MailBoxRxSetAndInit(&rxMessage2, 3, CAN_11BIT, 0x08, 2, 0x7FF);

}

void handleCANTx(void) {
    if (can_tx_flag) {

        dataCANTx(&txMessage1, txMessage1.CanID);  // Prepare CAN message for ID 0x03
        CAN_MessageSend(&txMessage1);  // Send the prepared message

        dataCANTx(&txMessage2, txMessage2.CanID);
        CAN_MessageSend(&txMessage2);  

        can_tx_flag = false; 
    }
}

void handleCANRx(void) {
    if (can_rx_flag) {

        dataCANRx(&rxMessage, rxMessage.CanID);
        rxMessage.newDataAvailable = 0;

        // dataCANRx(&rxMessage2, rxMessage2.CanID);
        // rxMessage2.newDataAvailable = 0;

        can_rx_flag = false;
    }
}

void ProtectionON(void) {
    GpioDataRegs.GPASET.bit.GPIO19 = 1;
}

void ProtectionOFF(void) {
    GpioDataRegs.GPACLEAR.bit.GPIO19 = 1; // Clear GPIO19 to 0
}

void ResetON(void) {
    GpioDataRegs.GPASET.bit.GPIO21 = 1; // Set GPIO21 to 1
}

void ResetOFF(void) {
    GpioDataRegs.GPACLEAR.bit.GPIO21 = 1; // Clear GPIO21 to 0
}

void PWM1_ON(void) {
    EALLOW;
    EPwm1Regs.TZCLR.all = 0x0000;
    EDIS;
}

void PWM1_OFF(void) {
    EALLOW;
    EPwm1Regs.TZCLR.all = 0xC000;
    EPwm1Regs.TZFRC.all = 0x0002;
    EDIS;
}

void PWM2_ON(void) {
    EALLOW;
    EPwm2Regs.TZCLR.all = 0x0000;
    EDIS;
}

void PWM2_OFF(void) {
    EALLOW;
    EPwm2Regs.TZCLR.all = 0xC000;
    EPwm2Regs.TZFRC.all = 0x0002;
    EDIS;
}

void PWM3_ON(void) {
    EALLOW;
    EPwm3Regs.TZCLR.all = 0x0000;
    EDIS;
}

void PWM3_OFF(void) {
    EALLOW;
    EPwm3Regs.TZCLR.all = 0xC000;
    EPwm3Regs.TZFRC.all = 0x0002;
    EDIS;
}

void PWM4_ON(void) {
    EALLOW;
    EPwm4Regs.TZCLR.all = 0x0000;
    EDIS;
}

void PWM4_OFF(void) {
    EALLOW;
    EPwm4Regs.TZCLR.all = 0xC000;
    EPwm4Regs.TZFRC.all = 0x0002;
    EDIS;
}

void PWM5_ON(void) {
    EALLOW;
    EPwm5Regs.TZCLR.all = 0x0000;
    EDIS;
}

void PWM5_OFF(void) {
    EALLOW;
    EPwm5Regs.TZCLR.all = 0xC000;
    EPwm5Regs.TZFRC.all = 0x0002;
    EDIS;
}

void PWM6_ON(void) {
    EALLOW;
    EPwm6Regs.TZCLR.all = 0x0000;
    EDIS;
}

void PWM6_OFF(void) {
    EALLOW;
    EPwm6Regs.TZCLR.all = 0xC000;
    EPwm6Regs.TZFRC.all = 0x0002;
    EDIS;
}

