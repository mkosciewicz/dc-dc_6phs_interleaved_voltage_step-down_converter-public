#include "hdrs/setupADC.h"
#include "F28x_Project.h"

void setupADC() {

    // Enable ADC clocks
    EALLOW;
    CpuSysRegs.PCLKCR13.bit.ADC_A = 1;
    CpuSysRegs.PCLKCR13.bit.ADC_C = 1;
    CpuSysRegs.PCLKCR13.bit.ADC_D = 1;

    // Set ADC Timer to 50MHz
    AdcaRegs.ADCCTL2.bit.PRESCALE = 6;
    AdccRegs.ADCCTL2.bit.PRESCALE = 6;
    AdcdRegs.ADCCTL2.bit.PRESCALE = 6;

    // Set ADC resolution and signal mode
    AdcSetMode(ADC_ADCA, ADC_RESOLUTION_12BIT, ADC_SIGNALMODE_SINGLE);
    AdcSetMode(ADC_ADCC, ADC_RESOLUTION_12BIT, ADC_SIGNALMODE_SINGLE);
    AdcSetMode(ADC_ADCD, ADC_RESOLUTION_12BIT, ADC_SIGNALMODE_SINGLE);

    // Power up all ADCs
    AdcaRegs.ADCCTL1.bit.ADCPWDNZ = 1;
    AdccRegs.ADCCTL1.bit.ADCPWDNZ = 1;
    AdcdRegs.ADCCTL1.bit.ADCPWDNZ = 1;

    // Delay for 1ms to allow ADC time to power up
    DELAY_US(1000);

    // Set pulse positions to late for all ADCs
    AdcaRegs.ADCCTL1.bit.INTPULSEPOS = 1;
    AdccRegs.ADCCTL1.bit.INTPULSEPOS = 1;
    AdcdRegs.ADCCTL1.bit.INTPULSEPOS = 1;

    // Set the acquisition frame length to 2us
    AdcaRegs.ADCSOC0CTL.bit.ACQPS = 9;
    AdcaRegs.ADCSOC1CTL.bit.ACQPS = 9;

    AdcdRegs.ADCSOC0CTL.bit.ACQPS = 9;
    AdcdRegs.ADCSOC1CTL.bit.ACQPS = 9;
    AdcaRegs.ADCSOC2CTL.bit.ACQPS = 9;

    AdccRegs.ADCSOC0CTL.bit.ACQPS = 9;
    AdccRegs.ADCSOC1CTL.bit.ACQPS = 9;
    AdccRegs.ADCSOC2CTL.bit.ACQPS = 9;

    // Set output channels for ADC
    AdcaRegs.ADCSOC0CTL.bit.CHSEL = ADC_CHANNEL_5; // Pinout for Uin (A5)
    AdcaRegs.ADCSOC1CTL.bit.CHSEL = ADC_CHANNEL_14; // Pinout for Uout (IN14)

    AdcdRegs.ADCSOC0CTL.bit.CHSEL = ADC_CHANNEL_4; // Pinout for ph1 (D4)
    AdcdRegs.ADCSOC1CTL.bit.CHSEL = ADC_CHANNEL_3; // Pinout for ph2 (D3)
    AdcaRegs.ADCSOC2CTL.bit.CHSEL = ADC_CHANNEL_3; // Pinout for ph3 (A3)

    AdccRegs.ADCSOC0CTL.bit.CHSEL = ADC_CHANNEL_4; // Pinout for ph4 (C4)
    AdccRegs.ADCSOC1CTL.bit.CHSEL = ADC_CHANNEL_3; // Pinout for ph5 (C3)
    AdccRegs.ADCSOC2CTL.bit.CHSEL = ADC_CHANNEL_2; // Pinout for ph6 (C2)

    // Set trigger sources for ADC
    AdcaRegs.ADCSOC0CTL.bit.TRIGSEL = 5;
    AdcaRegs.ADCSOC1CTL.bit.TRIGSEL = 5;

    AdcdRegs.ADCSOC0CTL.bit.TRIGSEL = 5; // Trigger on ePWM1 SOCA
    AdcdRegs.ADCSOC1CTL.bit.TRIGSEL = 7; // Trigger on ePWM2 SOCA
    AdcaRegs.ADCSOC2CTL.bit.TRIGSEL = 9; // Trigger on ePWM3 SOCA

    AdccRegs.ADCSOC0CTL.bit.TRIGSEL = 11; // Trigger on ePWM4 SOCA
    AdccRegs.ADCSOC1CTL.bit.TRIGSEL = 13; // Trigger on ePWM5 SOCA
    AdccRegs.ADCSOC2CTL.bit.TRIGSEL = 15; // Trigger on ePWM6 SOCA

    // Set INT1, INT2 or INT1 flag in each ADC peripheral on separate SOCs
    // Uin -> ADCA1
    AdcaRegs.ADCINTSEL1N2.bit.INT1SEL = 0;
    AdcaRegs.ADCINTSEL1N2.bit.INT1E = 1; // Enable INT flag
    AdcaRegs.ADCINTFLGCLR.bit.ADCINT1 = 1; // Clear flag

    // Uout -> ADCA
    AdcaRegs.ADCINTSEL1N2.bit.INT2SEL = 1;
    AdcaRegs.ADCINTSEL1N2.bit.INT2E = 1;
    AdcaRegs.ADCINTFLGCLR.bit.ADCINT2 = 1;

    // epwm1 -> ADCD4
    AdcdRegs.ADCINTSEL1N2.bit.INT2SEL = 0;
    AdcdRegs.ADCINTSEL1N2.bit.INT2E = 1;
    AdcdRegs.ADCINTFLGCLR.bit.ADCINT2 = 1;

    // epwm2 -> ADCD3
    AdcdRegs.ADCINTSEL3N4.bit.INT3SEL = 1;
    AdcdRegs.ADCINTSEL3N4.bit.INT3E = 1;
    AdcdRegs.ADCINTFLGCLR.bit.ADCINT3 = 1;

    // epwm3 -> ADCA3
    AdcaRegs.ADCINTSEL3N4.bit.INT4SEL = 2;
    AdcaRegs.ADCINTSEL3N4.bit.INT4E = 1;
    AdcaRegs.ADCINTFLGCLR.bit.ADCINT4 = 1;

    // epwm4 -> ADCC4
    AdccRegs.ADCINTSEL1N2.bit.INT2SEL = 0;
    AdccRegs.ADCINTSEL1N2.bit.INT2E = 1;
    AdccRegs.ADCINTFLGCLR.bit.ADCINT2 = 1;

    // epwm5 -> ADCC3
    AdccRegs.ADCINTSEL3N4.bit.INT3SEL = 1;
    AdccRegs.ADCINTSEL3N4.bit.INT3E = 1;
    AdccRegs.ADCINTFLGCLR.bit.ADCINT3 = 1;

    // epwm6 -> ADCC2
    AdccRegs.ADCINTSEL3N4.bit.INT4SEL = 2;
    AdccRegs.ADCINTSEL3N4.bit.INT4E = 1;
    AdccRegs.ADCINTFLGCLR.bit.ADCINT4 = 1;

    EDIS;
}

// ### EOF ###

