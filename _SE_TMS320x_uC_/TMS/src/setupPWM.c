#include "F28x_Project.h"
#include "hdrs/setupPWM.h"

void setupPWM() {

    // Setup TBCTL (Time-Base Control)
    EALLOW;

    CpuSysRegs.PCLKCR0.bit.TBCLKSYNC = 0;

    // Set timer period
    EPwm1Regs.TBPRD = EPWM_TIMER_TBPRD;
    EPwm2Regs.TBPRD = EPWM_TIMER_TBPRD;
    EPwm3Regs.TBPRD = EPWM_TIMER_TBPRD;
    EPwm4Regs.TBPRD = EPWM_TIMER_TBPRD;
    EPwm5Regs.TBPRD = EPWM_TIMER_TBPRD;
    EPwm6Regs.TBPRD = EPWM_TIMER_TBPRD;

    // Set compare values for duty cycle
    EPwm1Regs.CMPA.bit.CMPA = 0;
    EPwm2Regs.CMPA.bit.CMPA = 0;
    EPwm3Regs.CMPA.bit.CMPA = 0;
    EPwm4Regs.CMPA.bit.CMPA = 0;
    EPwm5Regs.CMPA.bit.CMPA = 0;
    EPwm6Regs.CMPA.bit.CMPA = 0;

    // Initialize phase to 0
    EPwm1Regs.TBPHS.bit.TBPHS = 0x0000;
    EPwm2Regs.TBPHS.bit.TBPHS = EPWM_PHASE_SHIFT ;
    EPwm3Regs.TBPHS.bit.TBPHS = EPWM_PHASE_SHIFT * 2.0;
    EPwm4Regs.TBPHS.bit.TBPHS = EPWM_TIMER_TBPRD;
    EPwm5Regs.TBPHS.bit.TBPHS = EPWM_PHASE_SHIFT * 2.0;
    EPwm6Regs.TBPHS.bit.TBPHS = EPWM_PHASE_SHIFT;

    EPwm1Regs.TBCTL.bit.SYNCOSEL = TB_CTR_ZERO;
    EPwm2Regs.TBCTL.bit.SYNCOSEL = TB_SYNC_IN;
    EPwm3Regs.TBCTL.bit.SYNCOSEL = TB_SYNC_IN;
    EPwm4Regs.TBCTL.bit.SYNCOSEL = TB_SYNC_IN;
    EPwm5Regs.TBCTL.bit.SYNCOSEL = TB_SYNC_IN;
    EPwm6Regs.TBCTL.bit.SYNCOSEL = TB_SYNC_IN;

    // Clear counter
    EPwm1Regs.TBCTR = 0x0000;
    EPwm2Regs.TBCTR = 0x0000;
    EPwm3Regs.TBCTR = 0x0000;
    EPwm4Regs.TBCTR = 0x0000;
    EPwm5Regs.TBCTR = 0x0000;
    EPwm6Regs.TBCTR = 0x0000;

    EPwm1Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN;
    EPwm1Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1;
    EPwm1Regs.TBCTL.bit.CLKDIV = TB_DIV1;
    EPwm1Regs.TBCTL.bit.PHSEN = TB_DISABLE;

    EPwm2Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN;
    EPwm2Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1;
    EPwm2Regs.TBCTL.bit.CLKDIV = TB_DIV1;
    EPwm2Regs.TBCTL.bit.PHSEN = TB_ENABLE;  // Enable phase loading for EPWMx
    EPwm2Regs.TBCTL.bit.PHSDIR = 0;

    EPwm3Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN;
    EPwm3Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1;
    EPwm3Regs.TBCTL.bit.CLKDIV = TB_DIV1;
    EPwm3Regs.TBCTL.bit.PHSEN = TB_ENABLE;
    EPwm3Regs.TBCTL.bit.PHSDIR = 0;

    EPwm4Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN;
    EPwm4Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1;
    EPwm4Regs.TBCTL.bit.CLKDIV = TB_DIV1;
    EPwm4Regs.TBCTL.bit.PHSEN = TB_ENABLE;
    EPwm4Regs.TBCTL.bit.PHSDIR = 0;

    EPwm5Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN;
    EPwm5Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1;
    EPwm5Regs.TBCTL.bit.CLKDIV = TB_DIV1;
    EPwm5Regs.TBCTL.bit.PHSEN = TB_ENABLE;
    EPwm5Regs.TBCTL.bit.PHSDIR = 1;

    EPwm6Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN;
    EPwm6Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1;
    EPwm6Regs.TBCTL.bit.CLKDIV = TB_DIV1;
    EPwm6Regs.TBCTL.bit.PHSEN = TB_ENABLE;
    EPwm6Regs.TBCTL.bit.PHSDIR = 1;

    // Setup TZCTL (Trip-Zone Control)
    EPwm1Regs.TZCTL.bit.TZA = TZ_FORCE_LO;
    EPwm2Regs.TZCTL.bit.TZA = TZ_FORCE_LO;
    EPwm3Regs.TZCTL.bit.TZA = TZ_FORCE_LO;
    EPwm4Regs.TZCTL.bit.TZA = TZ_FORCE_LO;
    EPwm5Regs.TZCTL.bit.TZA = TZ_FORCE_LO;
    EPwm6Regs.TZCTL.bit.TZA = TZ_FORCE_LO;

    // Load on shadow
    EPwm1Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
    EPwm1Regs.CMPCTL.bit.LOADAMODE = 0; // Load every CTR=Zero

    EPwm2Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
    EPwm2Regs.CMPCTL.bit.LOADAMODE = 0;

    EPwm3Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
    EPwm3Regs.CMPCTL.bit.LOADAMODE = 0;

    EPwm4Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
    EPwm4Regs.CMPCTL.bit.LOADAMODE = 0;

    EPwm5Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
    EPwm5Regs.CMPCTL.bit.LOADAMODE = 0;

    EPwm6Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
    EPwm6Regs.CMPCTL.bit.LOADAMODE = 0;

    EPwm1Regs.AQCTL.bit.SHDWAQAMODE = CC_SHADOW;
    EPwm1Regs.AQCTL.bit.LDAQASYNC = 0;
    EPwm1Regs.AQCTL.bit.LDAQAMODE = 1;

    EPwm2Regs.AQCTL.bit.SHDWAQAMODE = CC_SHADOW;
    EPwm2Regs.AQCTL.bit.LDAQASYNC = 0;
    EPwm2Regs.AQCTL.bit.LDAQAMODE = 1;

    EPwm3Regs.AQCTL.bit.SHDWAQAMODE = CC_SHADOW;
    EPwm3Regs.AQCTL.bit.LDAQASYNC = 0;
    EPwm3Regs.AQCTL.bit.LDAQAMODE = 1;

    EPwm4Regs.AQCTL.bit.SHDWAQAMODE = CC_SHADOW;
    EPwm4Regs.AQCTL.bit.LDAQASYNC = 0;
    EPwm4Regs.AQCTL.bit.LDAQAMODE = 1;

    EPwm5Regs.AQCTL.bit.SHDWAQAMODE = CC_SHADOW;
    EPwm5Regs.AQCTL.bit.LDAQASYNC = 0;
    EPwm5Regs.AQCTL.bit.LDAQAMODE = 1;

    EPwm6Regs.AQCTL.bit.SHDWAQAMODE = CC_SHADOW;
    EPwm6Regs.AQCTL.bit.LDAQASYNC = 0;
    EPwm6Regs.AQCTL.bit.LDAQAMODE = 1;


    // What actions when Carrier at ZRO
    EPwm1Regs.AQCTLA.bit.ZRO = AQ_NO_ACTION;
    EPwm2Regs.AQCTLA.bit.ZRO = AQ_NO_ACTION;
    EPwm3Regs.AQCTLA.bit.ZRO = AQ_NO_ACTION;
    EPwm4Regs.AQCTLA.bit.ZRO = AQ_NO_ACTION;
    EPwm5Regs.AQCTLA.bit.ZRO = AQ_NO_ACTION;
    EPwm6Regs.AQCTLA.bit.ZRO = AQ_NO_ACTION;

    // What actions when Carrier at PRD
    EPwm1Regs.AQCTLA.bit.PRD = AQ_NO_ACTION;
    EPwm2Regs.AQCTLA.bit.PRD = AQ_NO_ACTION;
    EPwm3Regs.AQCTLA.bit.PRD = AQ_NO_ACTION;
    EPwm4Regs.AQCTLA.bit.PRD = AQ_NO_ACTION;
    EPwm5Regs.AQCTLA.bit.PRD = AQ_NO_ACTION;
    EPwm6Regs.AQCTLA.bit.PRD = AQ_NO_ACTION;

    // Set PWMxA on event A, for up count
    EPwm1Regs.AQCTLA.bit.CAU = AQ_CLEAR;
    EPwm2Regs.AQCTLA.bit.CAU = AQ_CLEAR;
    EPwm3Regs.AQCTLA.bit.CAU = AQ_CLEAR;
    EPwm4Regs.AQCTLA.bit.CAU = AQ_CLEAR;
    EPwm5Regs.AQCTLA.bit.CAU = AQ_CLEAR;
    EPwm6Regs.AQCTLA.bit.CAU = AQ_CLEAR;

    // Clear PWMxA on event A, for down count
    EPwm1Regs.AQCTLA.bit.CAD = AQ_SET;
    EPwm2Regs.AQCTLA.bit.CAD = AQ_SET;
    EPwm3Regs.AQCTLA.bit.CAD = AQ_SET;
    EPwm4Regs.AQCTLA.bit.CAD = AQ_SET;
    EPwm5Regs.AQCTLA.bit.CAD = AQ_SET;
    EPwm6Regs.AQCTLA.bit.CAD = AQ_SET;

//-----------------------SOC_Sets-----------------------
    EPwm1Regs.ETSEL.bit.SOCAEN = 1;
    EPwm1Regs.ETSEL.bit.SOCASEL = 1;
    EPwm1Regs.ETSEL.bit.SOCASELCMP = 0;

    EPwm2Regs.ETSEL.bit.SOCAEN = 1;
    EPwm2Regs.ETSEL.bit.SOCASEL = 1;
    EPwm2Regs.ETSEL.bit.SOCASELCMP = 0;

    EPwm3Regs.ETSEL.bit.SOCAEN = 1;
    EPwm3Regs.ETSEL.bit.SOCASEL = 1;
    EPwm3Regs.ETSEL.bit.SOCASELCMP = 0;

    EPwm4Regs.ETSEL.bit.SOCAEN = 1;
    EPwm4Regs.ETSEL.bit.SOCASEL = 1;
    EPwm4Regs.ETSEL.bit.SOCASELCMP = 0;

    EPwm5Regs.ETSEL.bit.SOCAEN = 1;
    EPwm5Regs.ETSEL.bit.SOCASEL = 1;
    EPwm5Regs.ETSEL.bit.SOCASELCMP = 0;

    EPwm6Regs.ETSEL.bit.SOCAEN = 1;
    EPwm6Regs.ETSEL.bit.SOCASEL = 1;
    EPwm6Regs.ETSEL.bit.SOCASELCMP = 0;

    EPwm1Regs.ETPS.bit.SOCAPRD = 1;
    EPwm1Regs.ETPS.bit.SOCPSSEL = 0;

    EPwm2Regs.ETPS.bit.SOCAPRD = 1;
    EPwm2Regs.ETPS.bit.SOCPSSEL = 0;

    EPwm3Regs.ETPS.bit.SOCAPRD = 1;
    EPwm3Regs.ETPS.bit.SOCPSSEL = 0;

    EPwm4Regs.ETPS.bit.SOCAPRD = 1;
    EPwm4Regs.ETPS.bit.SOCPSSEL = 0;

    EPwm5Regs.ETPS.bit.SOCAPRD = 1;
    EPwm5Regs.ETPS.bit.SOCPSSEL = 0;

    EPwm6Regs.ETPS.bit.SOCAPRD = 1;
    EPwm6Regs.ETPS.bit.SOCPSSEL = 0;


    CpuSysRegs.PCLKCR0.bit.TBCLKSYNC = 1;

    EDIS;
}

