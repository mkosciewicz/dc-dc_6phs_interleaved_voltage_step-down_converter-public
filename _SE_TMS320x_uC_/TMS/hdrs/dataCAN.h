#ifndef HDRS_DATACAN_H_
#define HDRS_DATACAN_H_

#include "F28x_Project.h"
#include "setupCAN.h"

typedef volatile struct {
    float Current_1, Current_2, Current_3, Current_4, Current_5, Current_6;
    float Voltage_in, Voltage_out;
    float U_REF;
//    float phs_1_curr, phs_2_curr, phs_3_curr, phs_4_curr, phs_5_curr, phs_6_curr, Uin_vltg, Uout_vltg;
} CAN_DATA_STRUCT;

extern CAN_DATA_STRUCT dataADC;

int dataCANTx(struct CAN_TX_MESSAGE_STRUCT *msg, uint32_t CanID);
float dataCANRx(struct CAN_RX_MESSAGE_STRUCT *msg, uint32_t CanID);
void mailboxingCAN();
void handleCANTx();
void handleCANRx();

// Declare the On/Off functions
void ProtectionON();
void ProtectionOFF();
void PWM1_ON();
void PWM1_OFF();
void PWM2_ON();
void PWM2_OFF();
void PWM3_ON();
void PWM3_OFF();
void PWM4_ON();
void PWM4_OFF();
void PWM5_ON();
void PWM5_OFF();
void PWM6_ON();
void PWM6_OFF();
void ResetON();
void ResetOFF();
#endif /* HDRS_DATACAN_H_ */
