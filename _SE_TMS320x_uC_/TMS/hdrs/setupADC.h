#ifndef SETUP_ADC_H_
#define SETUP_ADC_H_

#include "F28x_Project.h"
#include "F2837xD_Adc_defines.h"

// Function Prototypes
void setupADC();

#endif /* SETUP_ADC_H_ */
