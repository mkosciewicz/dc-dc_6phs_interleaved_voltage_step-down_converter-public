#ifndef HDRS_SETUPISR_H_
#define HDRS_SETUPISR_H_

#define N 6 // Active phases
#define CURRENT_SCALE_RATIO (1.0/N)
void setupISR();

void TxTimerISR();
void RxTimerISR();

void adc_Uin_isr(void);
void adc_Uout_isr(void);

void adc_phs_1_isr(void);
void adc_phs_2_isr(void);
void adc_phs_3_isr(void);

void adc_phs_4_isr(void);
void adc_phs_5_isr(void);
void adc_phs_6_isr(void);
#endif
