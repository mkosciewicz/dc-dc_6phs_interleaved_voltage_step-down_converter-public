#ifndef HDRS_SETUPCAN_H_
#define HDRS_SETUPCAN_H_

#include "F28x_Project.h"

enum CAN_IDTYPE {
    CAN_11BIT=0,
    CAN_29BIT=1
};
// ======= Data structures for CAN messages =======

struct CAN_TX_MESSAGE_STRUCT{
    uint16_t IntMsgNum; //internal CPU message number (1-32)
    uint32_t CanID;
    uint16_t length;
    uint16_t data[8];
    uint32_t MessageCounter; // counts send messages for debugging purposes
    enum CAN_IDTYPE CAN_IdType;
};

struct CAN_RX_MESSAGE_STRUCT{
    uint16_t IntMsgNum;      // internal CPU message number (1-32) - must be unique for each message!
    uint32_t CanID;          //
    uint32_t CanIDMask;      // sets which bits in CanID will be used for filtering messages
    uint16_t length;         // number of bytes in message - set by the user
    uint16_t data[8];        // holds received data
    uint32_t MessageCounter; // counts received messages for debugging purposes
    enum CAN_IDTYPE CAN_IdType;
    uint16_t newDataAvailable :1;
};

// ======= Constants for CAN message configuration =======
#define CAN_MSG_ID 1 // The ID for CAN messages
#define CAN_TX_MSG_OBJ 1 // The object number for CAN transmit
#define DEVICE_SYSCLK_FREQ 200000000 // System clock frequency in Hz, replace with the correct value for your device
#define MSG_OBJ_TYPE_TRANSMIT 0x01 // Assuming this is an enum or a macro for message type
#define MESSAGE_DATA_SIZE 4
#define CAN_STANDARD_ID CAN_11BIT

// ======= Constants for CAN bit timing configuration =======
#define CAN_MAX_BIT_DIVISOR     (13)   // The maximum CAN bit timing divisor
#define CAN_MIN_BIT_DIVISOR     (5)    // The minimum CAN bit timing divisor
#define CAN_MAX_PRE_DIVISOR     (1024) // The maximum CAN pre-divisor
#define CAN_MIN_PRE_DIVISOR     (1)    // The minimum CAN pre-divisor
#define CAN_BTR_BRP_M           (0x3F) // Bit rate prescaler mask
#define CAN_BTR_BRPE_M          (0xF0000) // Extended bit rate prescaler mask
#define CAN_11BIT_ID_BIT_SHIFT  (18U)  // Shift for 11-bit IDs
#define CAN_29BIT_ID_BIT_SHIFT  (0U)   // Shift for 29-bit IDs

// ======= Global data used for CAN messages =======
extern unsigned char ucTXMsgData[4]; // TX Data
extern unsigned char ucRXMsgData[4]; // RX Data
extern uint32_t messageSize;         // Message Size (DLC)
extern volatile unsigned long msgCount;   // Counter for successful transmissions
extern volatile unsigned long errFlag;    // Flag for transmission errors
extern const uint16_t canBitValues[]; // Defined in the .c file
extern struct CAN_TX_MESSAGE_STRUCT txMessage;
extern struct CAN_RX_MESSAGE_STRUCT rxMessage;


// ======= Function prototypes for CAN operations =======
void setupCAN(void);
void CAN_PeripheralModuleInit(unsigned long bitrate);

__interrupt void CANA_interrupt0(void);

void CAN_MailBoxTxSetAndInit(struct CAN_TX_MESSAGE_STRUCT *CanTxMessage, uint16_t IntMsgNum, enum CAN_IDTYPE IdType, uint32_t Id, uint16_t length);
void CAN_MailBoxRxSetAndInit(struct CAN_RX_MESSAGE_STRUCT *CanRxMessage, uint16_t IntMsgNum, enum CAN_IDTYPE IdType, uint32_t Id, uint16_t length, uint32_t Mask);

void CAN_MessageRead(struct CAN_RX_MESSAGE_STRUCT *CanRxMessage);
void CAN_MessageSend(struct CAN_TX_MESSAGE_STRUCT *CanTxMessage);

uint32_t CAN_BitrateSet(uint32_t sourceClock, uint32_t bitRate);

void CAN_clearMailBoxes(void);

#endif // HDRS_SETUPCAN_H_
