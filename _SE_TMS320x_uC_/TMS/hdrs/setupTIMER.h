#ifndef HDRS_SETUPTIMER_H_
#define HDRS_SETUPTIMER_H_

void setupTxTimer(void);
void setupRxTimer(void);
void setupTIMER(void);

#endif /* HDRS_SETUPTIMER_H_ */
