#ifndef INITIALIZE_PI_CONTROLLERS_H
#define INITIALIZE_PI_CONTROLLERS_H

#include "F28x_Project.h"
#include "hdrs/reglsPI.h"

// Define constants here only if they are specific to PI controller initialization
#define voltageKP 0.4 // set date 20240409
#define voltageKI 2000 // set date 20240409

#define currentKP 0.1
#define currentKI 100.0

// #define KPI2 0.1
// #define KII2 100.0

// #define KPI3 0.1
// #define KII3 100.0

// #define KPI4 0.1
// #define KII4 100.0

// #define KPI5 0.1
// #define KII5 100.0

// #define KPI6 0.1
// #define KII6 100.0

#define TS 1e-5
#define REG_LOWER_LIM 0
#define VREG_UPPER_LIM_AMP 18
#define IREG_UPPER_LIM 1

// Declare the functions if they are specific to this module and not declared in "Pi_i.h"
void initReglsPI(void);

#endif // INITIALIZE_PI_CONTROLLERS_H
