#ifndef PWM_H_
#define PWM_H_
#include "F28x_Project.h"

// Declaration of the initPWM function
void setupPWM();

//PWM_Period
#define EPWM_TIMER_TBPRD  500.0 // Assuming a timer period for all PWMs
#define EPWM_PHASE_SHIFT  166.67

#endif /* PWM_H_ */
