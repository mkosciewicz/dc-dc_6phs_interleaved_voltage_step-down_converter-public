`include "dffasync.v"

module electric_protection (
  // --- IN_Ctrl_Signals ---
  input wire clk,
  input wire [6:1] PWM_IN,
  input wire [6:1] Currents_Comps,
  input wire Vin_Comp,
  input wire Vout_Comp,

  // --- IN_User_Signals ---
  input wire rst_bit,
  input wire test_bit,

  // --- OUT_Ctrl_Signals ---
  output wire [6:1] PWM_OUT,
  output wire [6:1] Enable_OUT,
  output wire [6:1] Enable_of_Comps,
  output wire RELAY_DC_PLUS,
  output wire RELAY_DC_MINUS,
  output wire RELAY_DC_FAN,
  output wire RELAY_DC_PRECHARGE
);

  // ----- Internal Signals ----- 
  wire [8:1] FLT_bus;
  wire [8:1] FLT_reg;
  wire test, test_reg;

  // ----- Assignments ----- 
  assign FLT_bus = {Vout_Comp, Vin_Comp, Currents_Comps};
  assign reset = rst_bit;
  assign test = ~test_bit;
  assign Enable_of_Comps = Currents_Comps;

  // ----- D-Type Flip-Flops for Latching Faults -----
  dffasync FLT_Latch_FF [8:1] (
    .D(1'b0),
    .clk(clk), 
    .enable(reset),    
    .async_set(|FLT_bus),
    .Q(FLT_reg)
  );

  dffasync Test_Latch_FF (
    .D(1'b0),
    .clk(clk), 
    .enable(reset),    
    .async_set(test),
    .Q(test_reg)
  );

  assign {PWM_OUT, Enable_OUT} = (|FLT_reg || test_reg) ? 12'b0 : {PWM_IN, PWM_IN};
  assign {RELAY_DC_PLUS, RELAY_DC_MINUS, RELAY_DC_FAN, RELAY_DC_PRECHARGE} = (|FLT_reg || test_reg) ? 4'b1111 : 4'b0000;

endmodule
