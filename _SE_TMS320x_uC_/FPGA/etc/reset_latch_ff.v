
// Flip-flop to latch the external reset signal
module reset_latch_ff(
    input wire clk,
    input wire external_reset, // User-triggered reset input
    input wire internal_reset_ack, // Internal signal to release reset latch
    output reg reset_latch = 0 // Output reset latch signal
);

always @(posedge clk) begin
    if (external_reset) begin
        // Set the latch when external reset is triggered
        reset_latch <= 1'b1;
    end
    else if (internal_reset_ack) begin
        // Reset the latch based on an internal condition acknowledging the reset has been handled
        reset_latch <= 1'b0;
    end
end

endmodule