// ### Flip-Flop ###
module rs_ff(
    input wire set,    // Set signal (e.g., fault condition detected)
    input wire reset,  // Reset signal (e.g., manual reset after fault condition is cleared)
    output reg q       // Output state (e.g., indicates a fault condition is active)
);

// As RS flip-flops are sensitive to both set and reset conditions, care must be taken to avoid both being high simultaneously, which is generally considered an illegal state for RS flip-flops.
initial begin
    q = 1'b0; // Or whatever your default state should be
end

always @(set or reset) begin
    if (reset) begin
        q <= 1'b0; // Reset condition has priority
    end
    else if (set) begin
        q <= 1'b1; // Set condition is only considered if reset is not active
    end
end

endmodule
