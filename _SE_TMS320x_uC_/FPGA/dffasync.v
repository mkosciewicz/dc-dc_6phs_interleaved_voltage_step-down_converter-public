module dffasync (
  D, clk, async_set, enable, Q
);
  input D; // Data input 
  input clk; // clock input 
  input async_set; // asynchronous reset low level 
  input enable; //  
  output reg Q; // output Q
  initial Q = 1'b0;
  
  always @(posedge clk or negedge async_set) begin
    if (async_set == 1'b0)
      Q <= 1'b1; 
    else if (enable)
      Q <= D; 
  end 

   
endmodule